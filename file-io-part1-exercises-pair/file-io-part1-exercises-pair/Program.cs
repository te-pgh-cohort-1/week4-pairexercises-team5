﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace file_io_part1_exercises_pair
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in the full filesystem path including the filename for a word and sentence count:");
            string userPath = Console.ReadLine();
            int numberOfWords = 0;
            int numberOfSentences = 0;
           // string directory = @"~\";
          //  string bookDirectory = @"book\";
            //string filename = "alices_adventures_in_wonderland.txt";
            //create the full path for the file in its directory.
            //need to be a string.
            //this needs to work on both of our computers after push pulled.
            //string fullPath = Path.Combine( bookDirectory, filename);
            try

            {
                //use stream reader to read the file begining to end.
                //might need to do it in increments. depending on size.
              
                using (StreamReader sr = new StreamReader(userPath))
                {
                    while (!sr.EndOfStream)
                    {  //split with the delimeters at every word.
                       //split with the delimeters at every . ! ?
                        string aliceBook = sr.ReadToEnd();
                        char[] wordSplits = { ' ', '_' };
                        string[] wordcount = aliceBook.Split(wordSplits, StringSplitOptions.RemoveEmptyEntries);
                        numberOfWords = wordcount.Length;

                        string[] sentenceSplits = { "! ","!\"", "? ","?\"", ". ",".\"" };
                        string[] sentenceCount = aliceBook.Split(sentenceSplits, StringSplitOptions.RemoveEmptyEntries);
                        numberOfSentences = sentenceCount.Length;

                        //C:\Users\Nico Cersosimo\book\alices_adventures_in_wonderland.txt

                    }
                }
            }
            catch(IOException e) 
            {
                Console.WriteLine("Couldn't read file, Please check filesystems path, and filename...");
                Console.Write(e.Message);
            }
            Console.WriteLine($"Number of words is: {numberOfWords}\nNumber of Sentences: {numberOfSentences}");
            Console.ReadLine();
            
           

        
























        }
    }
}
