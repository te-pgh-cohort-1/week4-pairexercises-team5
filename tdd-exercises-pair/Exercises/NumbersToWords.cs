﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Exercises
{
    public class NumbersToWords

    {
        public string[] under20 = new string[] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine","ten",
            "eleven","twelve", "thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen" };
        public string[] theTens = new string[] { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        

        public string NumboWords(int number)
        {
            string strNum = number.ToString();
            string wordAnswer = "";

            if (number == 0)
            {
                return "zero";
            }
            if ((number / 1000) > 0)
            {
                wordAnswer += NumboWords(number / 1000) + " thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                if (wordAnswer != "")
                {
                    wordAnswer += "and ";
                }
                wordAnswer += NumboWords(number / 100) + " hundred ";
                number %= 100;
            }
            if (number > 0)
            {
                if (wordAnswer != "")
                    wordAnswer += "and ";
            }
            if (number < 20)
                wordAnswer += under20[number];
            else
            {
                wordAnswer += theTens[number / 10];
                if (number % 10 > 0)
                {
                    wordAnswer += "-" + under20[number % 10];
                }
            }
            return wordAnswer.Trim();
        }
    }
}
