﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Exercises
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (numbers.Length < 1)
            {
                return 0;
            }
            else if (numbers.Length == 1)
            {
                int num = int.Parse(numbers);
                return num;
            }
            else if (numbers.Contains("/"))
            {
                char[] delimiters = { ',', '\n', '/', numbers[2] };
                string[] splitNumbers = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);               
                int sum = 0;
                foreach (string number in splitNumbers)
                {
                    sum += int.Parse(number);
                }
                return sum;
            }
            else
            {
                char[] delimiters = { ',', '\n' };
                string[] splitNumbers = numbers.Split(delimiters);
                int sum = 0;
                foreach (string number in splitNumbers)
                {
                    sum += int.Parse(number);
                }
                return sum;
            }

        }
    }
}
