﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Exercises;
using System.Linq;

namespace Exercises.Test
{
    [TestClass]
    public class NumbersToWordsTest
    {
        private NumbersToWords num2Word = new NumbersToWords();

        [TestMethod]
        public void Nums2WordsTest()
        {
            int[] OneDigit = { 0, 3, 7 };
            int[] TwoDigit = { 10, 14, 26 };
            int[] ThreeDigit = { 209, 300, 498 };
            int[] FourDigit = { 3004, 5026, 7111 };
            int[] FiveDigit = { 40000, 87654 };
            int[] SixDigit = { 500000, 803308, 999999 };

            string zeroResult = num2Word.NumboWords(OneDigit[0]);
            string threeResult = num2Word.NumboWords(OneDigit[1]);
            string sevenResult = num2Word.NumboWords(OneDigit[2]);
            string TwoDig1Result = num2Word.NumboWords(TwoDigit[0]);
            string TwoDig2Result = num2Word.NumboWords(TwoDigit[1]);
            string TwoDig3Result = num2Word.NumboWords(TwoDigit[2]);
            string ThreeDig1Result = num2Word.NumboWords(ThreeDigit[0]);
            string ThreeDig2Result = num2Word.NumboWords(ThreeDigit[1]);
            string ThreeDig3Result = num2Word.NumboWords(ThreeDigit[2]);
            string FourDig1Result = num2Word.NumboWords(FourDigit[0]);
            string FourDig2Result = num2Word.NumboWords(FourDigit[1]);
            string FourDig3Result = num2Word.NumboWords(FourDigit[2]);
            string FiveDig1Result = num2Word.NumboWords(FiveDigit[0]);
            string FiveDig2Result = num2Word.NumboWords(FiveDigit[1]);
            string sixDig1Result = num2Word.NumboWords(SixDigit[0]);
            string sixDig2Result = num2Word.NumboWords(SixDigit[1]);
            string sixDig3Result = num2Word.NumboWords(SixDigit[2]);

            Assert.AreEqual("zero", zeroResult, "Should return string zero as the number is 0");
            Assert.AreEqual("three", threeResult, "Should return string three as the number is 3");
            Assert.AreEqual("seven", sevenResult, "Should return string seven as the number is 7");
            Assert.AreEqual("ten", TwoDig1Result, "Should return string ten as the number is 10");
            Assert.AreEqual("fourteen", TwoDig2Result, "Should return string fourteen as the number is 14");
            Assert.AreEqual("twenty-six", TwoDig3Result, "Should return string twenty-six as the number is 26");
            Assert.AreEqual("two hundred and nine", ThreeDig1Result, "Should return string two-hundred and nine as the number is 209");
            Assert.AreEqual("three hundred", ThreeDig2Result, "Should return string three hundred as the number is 300");
            Assert.AreEqual("four hundred and ninety-eight", ThreeDig3Result, "Should return string four hundred and ninety eight as the number is 498");
            Assert.AreEqual("three thousand and four", FourDig1Result, "Should return string three thousand and four as the number is 3004");
            Assert.AreEqual("five thousand and twenty-six", FourDig2Result, "Should return string five thousand and twenty-six as dthe number is 5026");
            Assert.AreEqual("seven thousand and one hundred and eleven", FourDig3Result, "Should return string seven thousand and one hundred and eleven as the number is 7111");
            Assert.AreEqual("forty thousand", FiveDig1Result, "Should return string forty thousand the number is 40000");
            Assert.AreEqual("eighty-seven thousand and six hundred and fifty-four", FiveDig2Result, "Should return string eighty-seven thousand and six hundred and fifty-four the number is 87654");
            Assert.AreEqual("five hundred thousand", sixDig1Result, "Should return string five hundred thousand the number is 500000");
            Assert.AreEqual("eight hundred and three thousand and three hundred and eight", sixDig2Result, "Should return string eight hundred and three thousand and three hundred and eight the number is 803308");
            Assert.AreEqual("nine hundred and ninety-nine thousand and nine hundred and ninety-nine", sixDig3Result, "Should return string nine hundred and ninety-nine thousand and nine-hundred and ninety-nine the number is 999999");

        }
    }
}
