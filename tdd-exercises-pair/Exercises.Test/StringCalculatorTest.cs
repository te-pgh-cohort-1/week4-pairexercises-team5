using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Exercises;
using System.Linq;

namespace Exercises.Test
{
    [TestClass]
    public class StringCalculatorTest
    {
        private StringCalculator calc = new Exercises.StringCalculator();
        [TestMethod]
        public void CalculateAdd()
        {
            string empty = "";
            string one = "1";
            string multiple = "1,2";
            string threeNums = "1,5,7";
            string fournums = "1,10,11,3";
            string NewLine1 = "1\n2,3";
            string NewLine2 = "3\n5\n2,4";
            string Delimiter = "//;\n1;2";

            int resultEmpty = calc.Add(empty);
            int resultOne = calc.Add(one);
            int resultMultiple = calc.Add(multiple);
            int resultFourNums = calc.Add(fournums);
            int resultThreeNums = calc.Add(threeNums);
            int resultNewLine1 = calc.Add(NewLine1);
            int resultNewLine2 = calc.Add(NewLine2);
            int resultDelimiter = calc.Add(Delimiter);

            Assert.AreEqual(0, resultEmpty, "An empty string should return 0");
            Assert.AreEqual(1, resultOne, "Any single number should return the same number");
            Assert.AreEqual(3, resultMultiple, "Any group of numbers should return the sum of them all");
            Assert.AreEqual(13, resultThreeNums, "Any group of numbers should return the sum of them all, 1,5,7 should return 13");
            Assert.AreEqual(25, resultFourNums, "Any group of numbers should return the sum of them all,  1,10,11,3 should return 25");
            Assert.AreEqual(6, resultNewLine1, "numbers should be split at both commas and new lines and return the sum of all numbers");
            Assert.AreEqual(14, resultNewLine2, "numbers should be split at both commas and new lines and return the sum of all numbers");
            Assert.AreEqual(3, resultDelimiter, "numbers should be split at every comma, new line, slash, and newly declared delimiter and returns the sum of all remaining numbers");

        }
    }
}
