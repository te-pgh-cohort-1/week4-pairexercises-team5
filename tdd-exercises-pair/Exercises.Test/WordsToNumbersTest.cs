﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Exercises;
using System.Linq;

namespace Exercises.Test
{
    [TestClass]
    public class WordsToNumbersTest
    {
        private WordsToNumbers word2numbo = new WordsToNumbers();
        [TestMethod]
        public void wordToNumbo()
        {       //arrange
            string allNines = "nine hundred and ninety-nine thousand and nine hundred and ninety-nine";
            //act
            int result = word2numbo.word2Num(allNines);
            //assert
            Assert.AreEqual(999999, result, "Expected 999999 when that string was put in");
        }
    }
}
