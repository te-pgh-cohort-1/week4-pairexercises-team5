﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAndReplace
{
    public class FindAndReplaceProgram : FileVerification
    {
        static void Main(string[] args)
        {
            FileVerification checker = new FileVerification();

            Console.WriteLine("Please enter in the full filepath including the filename:");
            string userPath = Console.ReadLine();
            checker.VerifyFile(userPath);
            Console.WriteLine("Please enter a string from the file and what you would like to replace it with separated by a space (case-sensitive):");
            string words = Console.ReadLine();
            string[] replacement = words.Split(" ");
            Console.WriteLine("Please enter the filepath in which you would like your new edited version of your file to be saved:");
            string newPath = Console.ReadLine();
            checker.VerifyFile(newPath);
            //File.Create(newPath);
            try
            {
                using (StreamReader sr = new StreamReader(userPath))
                {
                    using (StreamWriter sw = new StreamWriter(newPath))
                    {
                        while (!sr.EndOfStream)
                        {
                            string file = sr.ReadToEnd();
                            string replace = file.Replace(replacement[0], replacement[1]);
                            sw.WriteLine(replace);
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Couldn't read file, Please check filesystems path, and filename...");
                Console.Write(e.Message);
            }
            Console.WriteLine("Great job! You successfully created a new edited version of your file. Press the 'enter' key to exit.");
            Console.ReadLine();
            //C:\Users\Nico Cersosimo\Book\alices_adventures_in_wonderland.txt
        }
    }
}
