﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAndReplace
{
    public class FileVerification
    {
        public string GetFilePath { get; set; }
        public bool IsValid = false;
        public void VerifyFile(string GetFilePath)
        {
            while (!IsValid)
            {
                if (File.Exists(GetFilePath))
                {
                    IsValid = true;
                }
                else
                {
                    Console.WriteLine("Filepath invalid. Please provide a valid path.");
                    GetFilePath = Console.ReadLine();
                }
            }
        }
    }
}
